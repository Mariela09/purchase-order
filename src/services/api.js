const SERVER_URL = 'https://eshop-deve.herokuapp.com/api/v2/orders'
const token = 'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJwUGFINU55VXRxTUkzMDZtajdZVHdHV3JIZE81cWxmaCIsImlhdCI6MTYwNTY0NDA0NzA1OH0.skfIY_7CAANkxmhoq37OI4jYRE8flx1ENq1v1VaRevJiroYNFQYz7Oy6hL1YZ1OJkevXSQFuLMHTqY0w6d5nPQ'
const orders = [
  {
    currency: 'MXN',
    status: {
      financial: 'financial'
    },
    name: 'Order 1',
    number: '1',
    dates: {
      createdAt: '2022-08-23T13:14:40.859340-05:00'
    },
    totals: {
      total: 1000
    },
    items: [
      {
        sku: 'pro-1',
        name: 'Blusas',
        quantity: 1,
        price: 1200
      },
      {
        sku: 'pro-2',
        name: 'Gorro',
        quantity: 3,
        price: 350
      }
    ],
    id: 0,
    reveal: false
  },
  {
    currency: 'MXN',
    status: {
      financial: 'financial'
    },
    name: 'Order 2',
    number: '2',
    dates: {
      createdAt: '2022-08-23T13:14:40.859340-05:00'
    },
    totals: {
      total: 1000
    },
    items: [{
      sku: 'la-1',
      name: 'Lapiz',
      quantity: 1,
      price: 5
    }],
    id: 1,
    reveal: false
  },
  {
    currency: 'MXN',
    status: {
      financial: 'financial'
    },
    name: 'Order 3',
    number: '3',
    dates: {
      createdAt: '2022-08-23T13:14:40.859340-05:00'
    },
    totals: {
      total: 1000
    },
    items: [{
      sku: 'ho-1',
      name: 'Hojas',
      quantity: 100,
      price: 250
    }],
    id: 2,
    reveal: false
  }
]
export default {
  getAll: async () => {
    const response = await fetch(SERVER_URL, {
      method: "GET",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': "Bearer" + token
      }
    })
    const items = await response.json()
    console.log(items)
    if(!items.orders) items.orders = [ ...orders ]
    return items
  }
}